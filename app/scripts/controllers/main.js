'use strict';

  var app = angular.module('angularLearningAppApp');


  // TODO: for now refator the factory here.
  // then externalize it into a separate folder
  app.factory('People', function () {

    var People = function () {
        this.list = [
        {
          id: 0,
          name: 'Leon',
          music: [
            'Rock',
            'Metal',
            'Dubstep',
            'Electro'
          ]
        },
        {
          id: 1,
          name: 'Harry',
          music: [
            'Rock',
            'Jpop',
            'Heavy Metal'
          ]
        }
      ];

      this.getList = function () {
        return this.list;
      };
    };

    return People;
  });


  app.controller('MainCtrl', function ($scope, $localStorage, People) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.inputValue = '';

    // Another example with a drop-down
    $scope.selectedPerson = 0;
    $scope.selectedGenre = null;

    $scope.people = $localStorage.$default({
        list:  new People()
    });
 

    $scope.newPerson = null;
    $scope.addNew = function() {
        if($scope.newPerson !== null && $scope.newPerson !== '') {
          $scope.people.list.push({
            id: $scope.people.list.length,
            name: $scope.newPerson,
            music: ['something shitty because they are new here']
          });
        }
    };


      $scope.$storage = $localStorage.$default({
          x: 42
      });
  });
